# [funsociety.dev](https://funsociety.dev)

**Links:**
* [blog](https://funsociety.dev/posts)
* [projects](https://funsociety.dev/projects)

## Add new content

Run following to create a new project MarkDown document
```shell
hugo new content/projects/xxx.md
```

Run following to create a new blog MarkDown document
```shell
hugo new content/posts/xxx.md
```